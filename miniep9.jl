using Test

function testes()

    #Parte 1
    @test matrix_pot([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot([1 2; 3 4], 2) == [7 10; 15 22]
    @test matrix_pot([3 5; 7 9], 3) == [552 760; 1064 1464]
    @test matrix_pot([1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1], 30) == [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]
    println("Os testes passaram para a parte 1.")

    #Parte 2
    @test matrix_pot_by_squaring([1 2; 3 4], 1) == [1 2; 3 4]
    @test matrix_pot_by_squaring([1 2; 3 4], 2) == [7 10; 15 22]
    @test matrix_pot_by_squaring([3 5; 7 9], 3) == [552 760; 1064 1464]
    @test matrix_pot_by_squaring([1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1], 30) == [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]
    println("Os testes passaram para a parte 2.")
    
end

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    prod = M
    while p > 1
        prod = multiplica(prod, M)
        p -= 1
    end
    return prod
end

function matrix_pot_by_squaring(M, p)
    mat_quad = multiplica(M, M)
    result = 0
    if p == 1
        result = M
    elseif (p % 2) == 0
        result = matrix_pot_by_squaring(mat_quad, p/2)
    elseif (p % 2) == 1
        odd_pot = matrix_pot_by_squaring(mat_quad, (p-1)/2)
        result = multiplica(M, odd_pot)
    end
    return result
end

using LinearAlgebra

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)
end

compare_times()
